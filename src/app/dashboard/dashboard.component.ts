import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public userDatails: any;
  
  constructor(
    private router: Router

  ) { }

  ngOnInit(): void {
    const storage = localStorage.getItem('google_auth')
    
    if(storage) {
      this.userDatails = JSON.parse(storage);
    }
  }

  signOut(): void {
    localStorage.removeItem('google_auth');
    this.router.navigateByUrl('/authorization').then();
  }
}
